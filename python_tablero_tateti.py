#!/usr/bin/python3

import string

linea_borde = '-'*33
linea_jugada = '| {} '
separador = '|'
linea_sin_jugada ='|'+' '*3+'|'+' '*3+'|'+' '*3+'|'
peones = 'pppppppp'
vacios = '        '

tablero = [['T', 'C', 'A', 'Q', 'R', 'A', 'C', 'T'], 
	[ x.upper() for x in peones ], 
	[ x for x in vacios ],
	[ x for x in vacios ],
	[ x for x in vacios ],
	[ x for x in vacios ],
	[ x.lower() for x in peones ],
	['t', 'c', 'a', 'q', 'r', 'a', 'c', 't']]

print(linea_borde)
for l in tablero:
	# print(linea_sin_jugada)
	for c in range(0, len(l)):
		print(linea_jugada.format(l[c]), end='')
	print(separador)
	# print(linea_sin_jugada)
	print(linea_borde)
