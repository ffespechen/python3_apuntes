# Uso de setters and getters

class Persona:
	def __init__(self, nombre, edad):
		self._nombre = nombre
		self._edad = edad

	def get_edad(self):
		return self._edad

	def set_edad(self, nueva_edad):
		if isinstance(nueva_edad, int) and nueva_edad > 0 and nueva_edad < 120:
			self._edad = nueva_edad

	def get_nombre(self):
		return self._nombre

	def __str__(self):
		return 'Persona.name = '+self._nombre +' edad = '+str(self._edad)

p1 = Persona('Charles Napier', 40)
print(p1)
p1.set_edad(20)
print(p1)


# Decoradores
# @property
# @<atributo>.setter
# @<atributo>.deleter

class Persona_decoradores:

	def __init__(self, nombre, edad):
		self._nombre = nombre
		self._edad = edad


	@property
	def edad(self):
		""" Edad de la persona """
		return self._edad

	@edad.setter
	def edad(self, nuevo_valor):
		if isinstance(nuevo_valor, int) and nuevo_valor>0 and nuevo_valor<120:
			self._edad = nuevo_valor

	@property
	def nombre(self):
		""" El nombre de la persona """
		return self._nombre


p2 = Persona_decoradores('Barbara Gordon', 26)

print('EDAD Previo a la asignación: ', p2.edad)
p2.edad = -1
print('EDAD Post intento de asignación -1: ', p2.edad)
p2.edad = 30
print('EDAD Post intento de asignación 30: ', p2.edad)

# Devuelve un error, la propiedad es de solo lectura
# p2.nombre = 'Selina Kyle'
print(p2.nombre)
