#!/usr/bin/python3

# Implementación de una Cola (Queue) con listas
# FIFO = First In First Out
cola = []
cola.append('Tarea 1')
print('Primera Tarea agregada : ', cola)
cola.append('Tarea 2')
cola.append('Tarea 3')
print('Estado actual de la Cola : ', cola)
elemento = cola.pop(0)
print('Elemento removido de la cola : ', elemento)
print('Estado de la cola luego de remover el primer elemento : ', cola)

# Implementación con una clase

class Cola:

	def __init__(self):
		self._elementos = []

	def __len__(self):
		""" Implementación para soportar el protocolo len() """
		return len(self._elementos)

	def __str__(self):
		return 'Clase Cola : '+str(self._elementos)

	def enqueue(self, elemento):
		self._elementos.append(elemento)

	def dequeue(self):
		return self._elementos.pop(0)

	def is_empty(self):
		""" Usamos la función propia ya implementada, por integridad de datos """
		return self.__len__() == 0

	def peek(self):
		return self._elementos[0]

cola1 = Cola()

cola1.enqueue('Tarea 1')
print('cola1 luego de agregado primer elemento : ', cola1)
cola1.enqueue('Tarea 2')
cola1.enqueue('Tarea 3')
print('cola1 con elementos agregados : ', cola1)
print('cola1.dequeue() : ', cola1.dequeue())
print('cola1.is_empty() : ', cola1.is_empty())
print('Estado de cola1 : ', cola1)
print('cola1.peek() : ', cola1.peek())

# Implement your own Stack class following the pattern used for the Queue class.
# The Stack class should provide
# • A push(element) method used to add an element to the Stack.
# • A pop() method to retrieve the top element of the Stack (this method
# removes that element from the stack).
# • A top() method that allows you to peek at the top element on the stack (it does
# not remove the element from the stack).
# • A __len__() method to return the size of the stack. This method also meets
# the len protocol requirements.
# • An is_empty() method that checks to see if the stack is empty.
# • A __str__() method used to convert the stack into a string.
