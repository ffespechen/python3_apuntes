import sys

print('sys.version : ', sys.version)
print('sys.maxsize : ', sys.maxsize)
print('sys.path : ', sys.path)
print('sys.platform : ', sys.platform)
print('sys.flags : ', sys.flags)
print('sys.copyright : ', sys.copyright)

print(dir(sys))


# Importando packages y módulos propios
# El directorio otorga nombre al package
# Incluir un archivo __init__.py para inicializaciones del package

import package1.modulo1
from package1.funciones import *

package1.modulo1.funcion_001()
funcion_2_p1()