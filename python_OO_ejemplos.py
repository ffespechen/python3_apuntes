# En Python TODO es un objeto (es decir, la instancia de una clase)

print(type(1))
print(type(2.5))
print(type('Python'))
print(type(0x100))
print(type([1, 2, 3, 4]))
print(type(True))
print(type({'key1': 1, 'key2': 2, 'key3': 3}))

# Definición de la clase Lata

class Lata:
	""" Una clase de ejemplo que representa latas de bebidas """

	cantidad_objetos = 0

	@classmethod
	def incrementar_objetos(cls):
		cls.cantidad_objetos += 1

	@staticmethod
	def metodo_estatico():
		print('Soy un método estático')

	def __init__(self, marca, contenido):
		Lata.incrementar_objetos()
		self.marca = marca
		self.contenido = contenido

	def __str__(self):
		return self.marca + ' contiene ' + str(self.contenido) + ' cm3 '


# Instancias de la clase Lata
lata_monster = Lata('Monster', 473)
lata_speed = Lata('Speed', 500)
lata_heineken = Lata('Heineken', 710)


# Mostramos los identificadores de las instancias
print('id(lata_monster) : ', id(lata_monster))
print('id(lata_speed) : ', id(lata_speed))
print('id(lata_heineken) : ', id(lata_heineken))

# Imprimiento nuestros objetos
print(lata_heineken)
print(lata_speed)

# Imprimir la cantidad de objetos creados
print('Cantidad de objetos instanciados: ', Lata.cantidad_objetos)

# Llamar a un método estático
Lata.metodo_estatico()

# Atributos intrínsecos

print('Atributos de Lata')
print(Lata.__name__)
print(Lata.__module__)
print(Lata.__bases__)
print(Lata.__dict__)
print(Lata.__doc__)