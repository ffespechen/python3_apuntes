class Cantidad:
	def __init__(self, value):
		self.value = value

	def __str__(self):
		return 'Cantidad.value = '+str(self.value)

	def __add__(self, other):
		nuevo_value = self.value + other.value
		return Cantidad(nuevo_value)

	def __sub__(self, other):
		nuevo_value = self.value - other.value
		return Cantidad(nuevo_value)

c1 = Cantidad(12)
c2 = Cantidad(8)

c3sum = c1 + c2
c4rest = c1 - c2

print(c1)
print(c2)
print('Resultado de c1 + c2', c3sum)
print('Resultadi de c1 - c2', c4rest)

'''

Operadores Numéricos
__add__(self, q2) Addition
__sub__(self, q2) Subtraction
__mul__(self, q2) Multiplication
__pow__(self, q2) Power
__truediv__(self, q2) Division
__floordiv__(self, q2) Floor Division
__mod__(self, q2) Modulo (Remainder)
__lshift__(self, q2) Bitwise Left Shift
__rshift__(self, q2) Bitwise Right Shift 


Operadores de comparación

__lt__(self, q1) Less than
__le__(self, q1) Less than or equal to
__eq__(self, q1) Equal to
__ne__(self, q1) Not Equal to
__gt__(self, q1) Greater than
__ge__(self, q1) Greater than or equal to 


Operator Expression Method

__and__(q1, q2) AND
__or__(q1, q2) OR
__xor__(q1, q2) XOR
__invert__() NOT
''' 
