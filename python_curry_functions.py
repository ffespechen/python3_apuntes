

def conversor(relacion, cantidad):
	return relacion * cantidad


def curry(funcion, relacion):
	return lambda x : funcion(relacion, x)


# Establezco las referencias a funciones anónimas con la relación de cambio adecuada
convertir_a_dolares = curry(conversor, 0.77)
convertir_a_euros = curry(conversor, 0.88)
convertir_a_yenes = curry(conversor, 0.12)


# Uso estas funciones aplicadas a los valores a convertir
print(convertir_a_dolares(10))
print(convertir_a_euros(20))
print(convertir_a_yenes(3.15))