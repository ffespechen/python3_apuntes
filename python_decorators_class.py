# Ejemplo de un decorador aplicado a una clase
# Implementación del Patrón Singleton

def singleton(clase):
	print('Patrón singleton para : ', clase)
	instancia = None

	def get_instance():
		
		nonlocal instancia
		
		if instancia is None:
			instancia = clase()

		return instancia

	return get_instance

@singleton
class Servicio(object):
	def print_it(self):
		print(self)


s1 = Servicio()
print(s1)
s2 = Servicio()
print(s2)


# Los decoradores se ejecutan en tiempo de importación

def logger(func):
	print('*'*20)
	print('Iniciando el logger')

	def inner():
		print('En el interior de inner llamando : ', func.__name__)
		func()
		print('En el interior de inner, ya llamada : ', func.__name__)

	print('Saliendo de logger')
	print('*'*20)

	return inner

@logger
def imprimir_esto():
	print('Función imprimir_esto()')

imprimir_esto()