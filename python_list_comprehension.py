#!/usr/bin/python3.8

# Formato : [ <expresion> for item in iterable <if optional_condition> ]

lista_resultado = [ (i, i**2) for i in range(0,1000) if i%2 != 0 ]

print(lista_resultado)


# Clases Collection

import collections

contador = collections.Counter(list('En un café se vieron por casualidad, cansados en el alma de tanto andar... Ella tenía un clavel en la mano.'))

print(contador.most_common())
