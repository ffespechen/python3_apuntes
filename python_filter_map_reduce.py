# Funciones de orden superior
# filter()
# map()
# functools.reduce()

datos = [1, 0, 5, 2, 3, 0, 0, 5, 0]

# Filter devuelve un iterable, que debe ser convertido al tipo de dato requerido
resultado0 = list(filter(lambda i : i==0, datos))
print(resultado0)

resultado1 = tuple(filter(lambda i : i!=0, datos))
print(resultado1)

