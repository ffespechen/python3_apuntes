# Una clase abastracta es aquella que no se puede instanciar
# Tiene propiedades y métodos sin implementar
# Puede utilizarse para establecer una interfaz común 
# en una jerarquía de herencias

from abc import ABCMeta, abstractmethod

class Figura(metaclass=ABCMeta):

	def __init__(self, id):
		self._id = id

	@abstractmethod
	def display(self):
		pass

	@property
	@abstractmethod
	def id(self):
		pass


# Si intentamos instanciar un objeto de esta clase, obtenemos un error
# my_figura = Figura(1)


class Circulo(Figura):

	def __init__(self, id):
		super().__init__(id)

	def display(self):
		print('Es un Circulo, con id : ', self._id)

	@property
	def id(self):
		return self._id


c = Circulo(253689)
print(c.id)
c.display()