# Ejercicio práctico del juego de la Dama en el Ajedrez

borde = '-'*33 


class Tablero:
	""" Tablero de juego de la Reina """
	def __init__(self, w=8, h=8):
		self._w = w
		self._h = h
		self._escaque = '|{:^3}'
		self._borde = '-'* (self._w*4+1)
		self._modelo = []
		self._construir_modelo(self._h, self._w)



	def draw(self, r, c):

		print('Posiciones alcanzadas por la Reina desde fila : ',
			r,
			' columna : ', 
			c)

		self._construir_modelo(r, c)

		print(self._borde)
		for i in range(self._h):
			for j in range(self._w):
				print(self._escaque.format(self._modelo[i][j]), end='')
			print('|')	
			print(self._borde)

	def _construir_modelo(self, r, c):

		vacio = ' '
		atacado = 'Q'
		self._modelo = []

		for i in range(self._h):
			self._modelo.append([])
			for j in range(self._w):
				self._modelo[i].append(vacio)

		if r>=self._h or c>=self._w or r<0 or c<0:
			return
		else:

			for i in range(0, self._h):
				self._modelo[i][c] = atacado

			for j in range(0, self._w):
				self._modelo[r][j] = atacado


			desp_x_der = c
			desp_y_arriba = r

			while (desp_y_arriba>=0 and desp_x_der<self._w):
				self._modelo[desp_y_arriba][desp_x_der] = atacado
				desp_y_arriba -= 1
				desp_x_der += 1

			desp_x_izq = c
			desp_y_arriba = r

			while (desp_y_arriba>=0 and desp_x_izq>=0):
				self._modelo[desp_y_arriba][desp_x_izq] = atacado
				desp_y_arriba -= 1
				desp_x_izq -= 1

			desp_x_der = c
			desp_y_abajo = r

			while (desp_y_abajo<self._h and desp_x_der<self._w):
				self._modelo[desp_y_abajo][desp_x_der] = atacado
				desp_y_abajo += 1
				desp_x_der += 1

			desp_x_izq = c
			desp_y_abajo = r

			while (desp_y_abajo<self._h and desp_x_izq>=0):
				self._modelo[desp_y_abajo][desp_x_izq] = atacado
				desp_y_abajo += 1
				desp_x_izq -= 1			


tablero = Tablero()

# Dibujar el Tablero y posicionar la Torre (fila, columna)
tablero.draw(3, 5)
