import string

# Las tuplas pueden crearse a partir de cualquier objeto iterable
tup1 = tuple(string.printable)
tup2 = (1, 3, 5, 7, 9, 11)

print(tup1)
print(tup2)

lista1 = [0, 2, 4, 6, 8, 10, 12]

print(lista1)
del(lista1[3])
print(lista1)
print(lista1.pop(3))
print(lista1)

# Conjuntos (desordenados y sin elementos repetidos)

conjunto1 = {'IronMan', 'Thor', 'Spiderman', 'Hulk', 'Spiderman', 'Spiderman', 'Captain America'}
print(conjunto1)

conjunto2 = set('Un gran poder implica una gran resposabilidad')
print(conjunto2)
print(len(conjunto2))

conjunto1.add('Vision')
print(conjunto1)
conjunto1.update(['Wanda', 'Antman', 'Star Lord'])
print(conjunto1)
conjunto1.remove('Spiderman') # Si el objeto no existe, lanza una Excepction
conjunto1.discard('Batman') # Si el objeto no existe, no lanza Excepciones ni genera errores
print(conjunto1)
