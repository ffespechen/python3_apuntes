# Las funciones de orden superior son aquellas que cumplen una o ambas condiciones:
# - Reciben como parámetro una referencia a otra función
# - Devuelven una función

def aplicar_funcion(parametro, funcion):
	"""
	Función de orden superior
	"""
	return funcion(parametro)

def sumar_uno(parametro):
	return parametro + 1

def restar_uno(parametro):
	return parametro - 1

def es_par(parametro):
	return True if (parametro%2 == 0) else False

def multiplicar_por_cien(parametro):
	return parametro * 100


print(aplicar_funcion(10, sumar_uno))
print(aplicar_funcion(10, restar_uno))
print(aplicar_funcion(10, es_par))
print(aplicar_funcion(10, multiplicar_por_cien))
