# A Decorator is a piece of code, that is used to mark a callable object (such as a
# function, method, class or object) typically to enhance or modify its behaviour
# (potentially replacing it). It thus decorates the original behaviour.

def logger(funcion):
	def interna():
		print('Llamando a : ', funcion.__name__)
		funcion()
		print('Función ', funcion.__name__, ' ya ejecutada.')
	return interna

def target():
	print('Función target() ejecutándose...')

# Aplicación explícita del decorador
d1 = logger(target)
d1()


# Aplicación con sintaxis de decorador '@'

@logger
def target1():
	print('Función target1() ejecutándose...')

target1()
