
# -- Uso de bloques try-except para atrapar excepciones

entrada = 'ABC'

try:

	resultado = int(entrada)

except Exception as e: # Si una exepción ocurre, se captura en este bloque
	print('Se ha capturado una excepción: ', e)

else: # Este bloque se ejecuta si y solo si NO SE DISPARA una excepción en el bloque try
	print('Bloque ELSE: No se han producido excepciones')

finally: # Este bloque se ejecuta siempre, hayan o no hayan ocurrido excepciones
	print('Bloque FINALLY: se ejecuta siempre, haya o no ocurrido una excepción')


# -- Lanzando excepciones

def funcion_con_excepciones():
	print('Función que lanza una excepción')
	raise Exception('Mensaje de excepción desde la funcion()')

try:
	print('Comienza el bloque try')
	funcion_con_excepciones()
except Exception as e:
	print('Exepción atrapada: ', e)
finally:
	print('Fin del bloque try')


# -- Definir un tipo propio de Excepciones

class InvalidDatoPersonalizado(Exception):
	""" Excepción personalizada """

	def __init__(self, valor):
		self.valor = valor

	def __str__(self):
		return 'InvalidDatoPersonalizado: (' + str(self.valor) + ')'
