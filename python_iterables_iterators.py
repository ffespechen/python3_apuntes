# Un iterable es un objeto que devuelve un iterador, sea él mimso u otro objeto
# __iter__ es el método a implementar para ser iterable
# __next__ es el método a implementar para ser iterador
# cuando el iterador llega al último elemento, devuelve la excepción StopIteration
import string 

class Pares(object):

	def __init__(self, limite):
		self.limite = limite
		self.valor = 0

	# Con la definición del método __iter__ la clase se hace iterable
	def __iter__(self):
		return self

	# Con la definición del método __next__ la clase se hace iterador
	def __next__(self):
		if self.valor > self.limite:
			raise StopIteration
		else:
			valor_a_retornar = self.valor
			self.valor += 2
			return valor_a_retornar


print('Comenzando -- NÚMEROS PARES')
for i in Pares(30):
	print(i, end=', ')
print('Hecho -- NÚMEROS PARES')


class Letras:

	def __init__(self, tipos=''):
		""" m = minúsculas / M = mayúsculas / Otro valor = todas """
		
		if tipos == 'm':
			self.letras = string.ascii_lowercase
		elif tipos == 'M':
			self.letras = string.ascii_uppercase
		else:
			self.letras = string.ascii_letters
		
		self.pos = 0
		self.elementos = len(self.letras)

	def __iter__(self):
		return self

	def __next__(self):
		if self.pos >= self.elementos:
			raise StopIteration
		else:
			letra_a_devolver = self.letras[self.pos]
			self.pos += 1
			return letra_a_devolver

print('Comenzando -- LETRAS')
for l in Letras():
	print('Letra : ', l, end=' | ')
print()
print('Finalizado -- LETRAS')
