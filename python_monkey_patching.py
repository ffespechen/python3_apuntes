# Monkey Patching se basa en el concepto de poder agregar comportamientos
# a un objeto en tiempo de ejecución, para cumplir algún requerimiento 
# que el tipo de datos originalmente no tenía

class Bolsa:

	def __init__(self):
		self.data = [1, 2, 3, 4]

	def __str__(self):
		return 'Objeto de la clase Bolsa, datos: ' + str(self.data)

	def __getitem__(self, pos):
		print('-- Método __getitem__ ejecutándose --')
		return self.data[pos]

	# Este método especial 'captura' los llamados a métodos o atributos que la clase no tiene
	# Si necesito que maneje llamados a métodos, debo devolver una referencia a un método self.default()
	# Otros métodos disponibles son __getattribute__(self, atribute) y __setattr__(self, atribute, value)
	def __getattr__(self, atribute):
		print(' -- Método __getattr__ invocado : ', atribute)
		return 'default'


# Agrego el comprtamiento para devolver la longitud (len) del objeto

def get_longitud(self):
	print('-- Función get_longitud ejecutándose --')
	return len(self.data)

# Aplico el principio de Monkey Patching, asignado a un método especial de la clase 
# una referencia a la función

Bolsa.__len__ = get_longitud

# Puedo agregar atributos (propiedades) en tiempo de ejecución

Bolsa.name = ''



b = Bolsa()

b.name = 'Bolsa 1 instancia'
print(b)
print(b.name)
print(b[2])
print(len(b))

# Invocando métodos o atributos que la clase no tiene

pqne = b.propiedad_que_no_existe
print(pqne)


