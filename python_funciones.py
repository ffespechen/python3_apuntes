def funcion_params_arbitrarios( *args ):
	"""
	Ejemplo de función con una cantidad arbitraria de parámetros posicionales
	"""
	pos = 0
	for arg in args:
		print('Parámetro en posición [{:^4}] tiene el valor {}'.format(pos, arg))
		pos += 1


def funcion_params_clave( **kwargs ):
	"""
	Ejemplo de función con parámetros con palabra clave
	"""
	for clave in kwargs.keys():
		print('Clave : [{:^12}] Valor : |{:^20}|'.format(clave, kwargs[clave]))



funcion_params_arbitrarios( 'Shrek', 'Fiona', 'Burro', 'Gato con Botas', 'Pinocho')


funcion_params_clave(batman='Bruce Wayne', gatubela='Selina Kyle', penguin='Oswald Cobblepot')

