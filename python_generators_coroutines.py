# Los Generadores son funciones especiales que pueden 'generar' una serie de valores
# para ser iterados a demanda
# Lo único que hace una función un Generador es el uso de la palabra reservada yield

from timeit import default_timer
import random
import time


def funcion_generadora():
	""" Devuelve infinitos números enteros pares menores que 100 """
	while True:
		print(default_timer())
		yield 2*random.randint(1,50)


## Para imprimir un listado infinito
#for i in funcion_generadora():
#	print(i)
#	time.sleep(1)

generador = funcion_generadora()

# Los generadores son objetos iterables
print(next(generador))
print(next(generador))
print(next(generador))
print(next(generador))


# Las Corutinas son consumidoras de datos generados por alguien/algo más
# Utilizan las funciones y palabras claves
# - yield
# - next()
# - send()
# - close()

def encontrar(palabra):

	print('Buscando -> ', palabra)

	try:
		while True:
			linea = (yield)
			if palabra in linea:
				print('Palabra -> ', palabra, ' encontrada en : ', linea)
	except GeneratorExit:
		print('Saliendo de la corutina')

# Guardo la referencia a la función
enc = encontrar('Hola')

# Inicializo la corutina
next(enc)

# Envío la info a procesar
enc.send('Hola Mundo!')
enc.send('Hello World')
enc.send('Qué lindo cuando te saludan con un "Hola"')
enc.send('Aquí no está')

# Finalizo la corutina - Se dispara la excepción GeneratorExit
enc.close()
